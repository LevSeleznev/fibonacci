Алгоритм Фибоначчи
==================

Рассматривается 2 способа вычисления числа из последовательности Фибоначчи по его позиции.

1. C помощью цикла:
```
int n = 10;
long resultWhile = fibonacciWhile(n);
```
2. C помощью рекурсии:
```
int k = 10;
long resultRecursion = fibonacciRecursion(k);
```