/**
 * Реализация алгоритма Фибоначчи
 *
 * @version 1.0
 * @autor Lev Seleznev
 */
public class Main {
    /**
     * Вычисление числа Фибоначчи с помощью цикла (потом вспомнил, что нужна рекурсия, было жалко удалять =))
     *
     * @param n - позиция числа в последовательности Фибоначчи
     * @return - возвращает число, стоящее на определённой позиции в последовательности Фибоначчи
     * @see Main#fibonacciRecursion(int)
     */
    public static long fibonacciWhile(int n) {
        if (n <= 1) {
            return n;
        }
        int i = 2;
        long result = 0;
        long first = 0;
        long second = 1;
        while (i <= n) {
            result = first + second;
            first = second;
            second = result;
            i += 1;
        }
        return result;
    }

    /**
     * Вычисление числа Фибоначчи с помощью рекурсии
     *
     * @param n - позиция числа в последовательности Фибоначчи
     * @return - возвращает число, стоящее на определённой позиции в последовательности Фибоначчи
     * @see Main#fibonacciWhile(int)
     */
    public static long fibonacciRecursion(int n) {
        if (n <= 1) {
            return n;
        }
        return fibonacciRecursion(n - 1) + fibonacciRecursion(n - 2);
    }

    public static void main(String[] args) {
        int n = 10;
        long resultWhile = fibonacciWhile(n);
        System.out.println("While: " + resultWhile);

        int k = 10;
        long resultRecursion = fibonacciRecursion(k);
        System.out.println("Recursion: " + resultRecursion);
    }
}
